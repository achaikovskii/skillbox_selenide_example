package module03;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class SelenideIntro {
    @Test
    void intershopLocatorsTest(){
        open("http://intershop2.skillbox.ru/");
        $x("//footer").scrollTo().shouldBe(visible);
        $("#menu-primary-menu").scrollTo().shouldBe(visible);
        $(By.className("custom-html-widget")).shouldBe(visible);
        $("button.searchsubmit[type='submit']").shouldBe(visible);
        $x("//footer//*[@id='pages-2']//*[text()='Бонусная программа']").scrollTo().shouldBe(visible);
    }

    @Test
    void shopBooksLocatorTest(){
        open("http://qajava.skillbox.ru/");
        $("[test-info='about-us']").scrollTo().shouldBe(visible);
        $x("(//a[text()='Бестселлеры'])[2]").scrollTo().shouldBe(visible);
        $x("//a[contains(text(), 'Доставка')]").shouldBe(visible);
        $x("//*[@class='menu-main']//a[@href='checkout.html']").shouldBe(visible);
        $x("//*[text()='Атлант расправил плечи']/parent::*").shouldBe(visible);
        $x("//*[text()='Атлант расправил плечи']//ancestor::*[@class='book-info']").shouldBe(visible);
        $x("//*[text()='Атлант расправил плечи']//ancestor::*[@class='book-info']/following-sibling::*[2]").shouldBe(visible);
    }

    @Test
    void catsPageTest(){
        open("http://qajava.skillbox.ru/module04/lesson2/");
        $x("//button[@id='write-to-me' and text()='Напишите мне']").shouldBe(visible);
        $("button[id='write-to-me']").shouldBe(visible);
    }
}
