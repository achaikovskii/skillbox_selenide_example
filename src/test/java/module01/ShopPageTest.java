package module01;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class ShopPageTest {
    private String url = "http://intershop.skillbox.ru/";
    private String searchText = "Машина";
    private String expectedSearchText = "машина";


    @Test
    public void checkResultSearch(){
        open(url);
        $(".search-field").setValue(searchText).pressEnter();

        // Убедиться, что первый из найденных результатов содержит слово «машина».
        $$(".wc-products h3").first().shouldHave(text(searchText));
        // Дополнительно к прошлому заданию убедитесь, что слово «машина» в первом результате написано
        // маленькими буквами (то есть Машина или МАШИНА — ошибка, машина — верно).
        $$(".wc-products h3").first().shouldHave(Condition.partialTextCaseSensitive(expectedSearchText));
        // Убедитесь в тесте, что поиск по слову «машина» находит три или больше элементов.
        $$(".product").shouldHave(CollectionCondition.sizeGreaterThan(2));
    }
}
