package module04;

import com.codeborne.selenide.*;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class LanguagePageTest {
    private String url = "https://lm.skillbox.cc/qa_tester/module08/practice2/";

    private WebDriver driver = new ChromeDriver();
    @Test
    public void checkHoverDate() {
        Configuration.browserSize = "1920x1080";
        open(url);
        $("[name='name']").setValue("Ivan");
        sleep(2000); // добавлено только из-за требований в задаче
        $("[name='last__name']").setValue("Ivanov");
        sleep(2000); // добавлено только из-за требований в задаче
        $x("(//span[@class='select2-selection__rendered'])[2]").click();
        sleep(2000); // добавлено только из-за требований в задаче
        $$(".select2-results .select2-results__option").find(text("1987")).click();
        sleep(2000); // добавлено только из-за требований в задаче
        $x("(//label[@class='jpn']//span)[1]").click();
        sleep(2000); // добавлено только из-за требований в задаче
        $x("//label[@class='rus']//span").scrollTo().click();
        sleep(2000); // добавлено только из-за требований в задаче
        $("[name='last__name']").append("-сан");
    }
}
