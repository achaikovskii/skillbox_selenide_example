package module04;

import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Selenide.*;

public class CallBackTest {
    private String url = "http://qa.skillbox.ru/module11/practice/feedbacksingle/";
    @Test
    public void checkHoverDate(){
        open(url);
        $("[name='datepicker']").click();
        $x("//span[text()='13']").hover();
        sleep(5000);
    }
}
