package module02;

import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class InterShopTest {
    private String url = "http://intershop2.skillbox.ru/";
    @Test
    public void checkVisibleTitles(){
        open(url);
        $x("//div[@id='menu']//a[contains(@href, 'my-account')]").click();

        // Убедиться, что виден заголовок h2 «Мой Аккаунт».
        $(".post-title").shouldBe(visible);
        $(".custom-register-button").click();

        // Убедиться, что виден заголовок h2 «Регистрация».
        $(".post-title").shouldBe(visible);
        $("#accesspress-breadcrumb a").click();

        // Убедиться, что заголовок h2 "Регистрация" больше не виден.
        $(".post-title").shouldNotBe(visible);
    }

    @Test
    public void checkTransitions(){
        open(url);

        $x("//div[@id='menu']//a[contains(@href, 'my-account')]").click();
        $(".custom-register-button").click();
        $("#accesspress-breadcrumb a").click();

        // Убедиться, что заголовок h2 "Регистрация" больше не виден.
        $(".post-title").shouldNotBe(visible);
    }

    @Test
    public void checkTextInTag(){
        open("https://lm.skillbox.cc/qa_tester/module07/homework1/");

        // тест падает из-за выбора невидимого элемента (требование задания)
        //$("main #finish").shouldHave(text("Телефон"));

        $("main #page_1").shouldHave(text("Телефон"));
    }
}
